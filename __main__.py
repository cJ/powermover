#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-powermover@zougloub.eu> & contributors
# SPDX-License-Identifier: GPL-3.0-only
# Dispatcher

import logging
import string
import subprocess
import os

from pathlib import Path

import gi
import ruamel.yaml

from rack.dict_sqlite3 import PersistentDict

gi.require_version('Gtk', '4.0')
gi.require_version('Gdk', '4.0')

from gi.repository import Gdk, Gtk, Gio, Pango, GLib, GObject


logger = logging.getLogger(__name__)


def load_config():
	XDG_CONFIG_ROOT = os.path.expanduser("~/.config")
	CONFIG_PATH = os.environ.get("POWERMOVER_CONFIG", os.path.join(XDG_CONFIG_ROOT, "powermover.yml"))
	yaml = ruamel.yaml.YAML()
	with open(CONFIG_PATH, 'r') as f:
		return yaml.load(f)



class FileItem(GObject.Object):
	path = GObject.Property(type=str)
	name = GObject.Property(type=str)
	extension = GObject.Property(type=str)
	modification_date = GObject.Property(type=float)
	size = GObject.Property(type=float)
	preview = GObject.Property(type=str)
	proposal = GObject.Property(type=str)
	def __str__(self):
		return f"(source {self.path})"


class DestinationItem(GObject.Object):
	name = GObject.Property(type=str)
	display_name = GObject.Property(type=str)
	path = GObject.Property(type=str)
	parent = GObject.Property(type=GObject.TYPE_PYOBJECT)
	def __str__(self):
		return f"(destination {self.name})"


class PowerMover(Gtk.ApplicationWindow):
	def __init__(self, app):
		super().__init__(application=app, title="PowerMover (initializing)")
		self.set_default_size(1920, 1080)

		# TODO is it worth it?
		self.selected_file = None

		self.config = load_config()
		self.source_folder = Path(self.config['source']).expanduser()
		self.destinations = self.config['destinations']
		self.ui_config = self.config.get('ui', {})

		paned = Gtk.Paned.new(Gtk.Orientation.HORIZONTAL)
		self.set_child(paned)

		# Left pane
		left_pane = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

		self.file_store = Gio.ListStore(item_type=FileItem)
		self.file_view = Gtk.ColumnView()
		sorter = self.file_view.get_sorter()
		self.sort_file_store = Gtk.SortListModel(model=self.file_store, sorter=sorter)

		self.file_selection = Gtk.SingleSelection(model=self.sort_file_store)
		self.file_view.set_model(self.file_selection)

		self.file_view.set_hexpand(True)
		self.file_view.set_vexpand(True)
		self.add_gesture_click(self.file_view, self.on_file_double_click)

		file_list_font = self.ui_config.get('file-list-font', 'Monospace 12')
		file_list_name_width_text = self.ui_config.get('file-list-name-width',
		 'This file name should be long enough')
		file_list_preview_width_text = self.ui_config.get('file-list-preview-width',
		 "This is a document about something. The description can take some room.",
		)
		file_list_extension_width_text = self.ui_config.get('file-list-extension-width',
		 ".html5")

		font_desc = Pango.FontDescription(file_list_font)
		self.file_view_font_desc = font_desc

		column_titles = ["Name", "Extension", "Modification Date", "Size", "Preview", "Proposal"]
		column_widths = {
		 "Name": self.calculate_text_width(file_list_name_width_text, font_desc),
		 "Preview": self.calculate_text_width(file_list_preview_width_text, font_desc),
		 "Extension": self.calculate_text_width(file_list_extension_width_text, font_desc),
		}

		self.columnviews = dict()

		for i, column_title in enumerate(column_titles):
			factory = Gtk.SignalListItemFactory()
			factory.connect("setup", self.setup_factory, column_title)
			factory.connect("bind", self.bind_factory, column_title)
			column = Gtk.ColumnViewColumn(title=column_title, factory=factory)
			if column_title in column_widths:
				column.set_fixed_width(column_widths[column_title])
			column.set_resizable(True)
			column.set_sorter(self.create_sorter(column))
			self.file_view.append_column(column)
			self.columnviews[column_title] = column

		self.file_view.sort_by_column(self.columnviews["Extension"], Gtk.SortType.ASCENDING)

		scrollable_left = Gtk.ScrolledWindow()
		scrollable_left.set_child(self.file_view)
		scrollable_left.set_hexpand(True)
		scrollable_left.set_vexpand(True)
		left_pane.append(scrollable_left)

		right_pane = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

		# Create a list (tree) view for destinations
		self.dir_store = Gio.ListStore(item_type=DestinationItem)
		self.dir_model = Gtk.TreeListModel.new(self.dir_store,
		 passthrough=False,
		 autoexpand=True,
		 create_func=self.create_tree_func,
		)

		self.dir_selection = Gtk.SingleSelection(model=self.dir_model)

		self.dir_view = Gtk.ListView(model=self.dir_selection, factory=self.create_tree_factory())

		self.add_gesture_click(self.dir_view, self.on_directory_double_click)

		scrollable_right = Gtk.ScrolledWindow()
		scrollable_right.set_child(self.dir_view)
		scrollable_right.set_hexpand(True)
		scrollable_right.set_vexpand(True)
		right_pane.append(scrollable_right)

		# Add panes to the paned widget
		paned.set_start_child(left_pane)
		paned.set_end_child(right_pane)

		# TODO compute from ratio
		paned.set_position(1600)

		self.dir_selection.connect("selection-changed", self.on_directory_selected)
		self.file_selection.connect("selection-changed", self.on_file_selected)

		self.populate_src()
		self.shortcuts = self.generate_shortcuts()

		# TODO rename, it's not path
		self.shortcut_to_path = dict()

		self.populate_dst()
		key_controller = Gtk.EventControllerKey()
		key_controller.connect("key-pressed", self.on_key_pressed)
		key_controller.set_propagation_phase(Gtk.PropagationPhase.CAPTURE)
		self.add_controller(key_controller)

		self.expand_all_tree_rows()

		self.apply_css()

	def view(self, path):
		"""
		"""
		subprocess.run(["xdg-open", path])


	def get_preview(self, path):
		"""
		"""

		db_location = self.config.get("db-location", os.path.expanduser("~/.cache/powermover"))

		with PersistentDict(db_location) as d:
			try:
				return d[path][:100]
			except:
				cmd = ["lesspipe", path]

				res = subprocess.run(cmd,
				 stdout=subprocess.PIPE,
				 stderr=subprocess.PIPE,
				)
				try:
					ret = res.stdout.decode()
				except:
					ret = "(no preview)"

				d[path] = ret
				return ret[:100]

	def apply_css(self):
		"""
		Make file list clearer
		"""
		css = b"""
		listview row:not(:selected):nth-child(even) {
			background-color: #f0f0f0;
		}
		"""
		css_provider = Gtk.CssProvider()
		css_provider.load_from_data(css)
		Gtk.StyleContext.add_provider_for_display(
		 Gdk.Display.get_default(),
		 css_provider,
		 Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
		)

	def expand_all_tree_rows(self):
		"""
		Expand destinations
		"""
		def recursive_expand(model):
			for i in range(model.get_n_items()):
				row = model.get_item(i)
				if isinstance(row, Gtk.TreeListRow):
					row.set_expanded(True)
					if row.get_children():
						recursive_expand(row.get_children())

		recursive_expand(self.dir_model)

	def generate_shortcuts(self):
		keys = list(string.digits) + list(string.ascii_lowercase)
		return {index: keys[index] for index in range(len(keys))}

	def on_key_pressed(self, controller, keyval, keycode, state):
		logger.debug("On key pressed %s", keyval)
		if keyval in (Gdk.KEY_Return, Gdk.KEY_KP_Enter):
			self.view_selected()
		elif keyval == Gdk.KEY_space:
			self.apply_proposed_destination()
		elif keyval in range(Gdk.KEY_0, Gdk.KEY_9 + 1):
			index = keyval - Gdk.KEY_0
			logger.info("Index %s", index)
			self.move_to_shortcut(index)
		elif keyval in range(Gdk.KEY_KP_0, Gdk.KEY_KP_9 + 1):
			index = keyval - Gdk.KEY_KP_0
			logger.info("Index %s", index)
			self.move_to_shortcut(index)
		elif keyval in range(Gdk.KEY_a, Gdk.KEY_z + 1):
			index = keyval - Gdk.KEY_a + 10
			logger.info("Index %s", index)
			self.move_to_shortcut(index)
		elif keyval == Gdk.KEY_Escape:
			self.close()

	def view_selected(self):
		pos = self.file_selection.get_selected()
		logger.info("View %s", pos)
		if pos is not None:
			item = self.file_selection.get_item(pos)
			self.view(item.path)

	def apply_proposed_destination(self):
		pos = self.file_selection.get_selected()
		logger.info("Apply proposed destination for %s", pos)
		if pos is not None:
			item = self.file_selection.get_item(pos)
			self.move(item, item.proposal)

	def move_to_shortcut(self, index):
		pos = self.file_selection.get_selected()
		if pos is None:
			return

		item = self.file_selection.get_item(pos)

		try:
			path = self.shortcut_to_path[index]
		except KeyError:
			logger.info("Ignore bad destination shortct %s", index)
			return
		self.move(item, path)

	def add_gesture_click(self, widget, callback, data=None):
		gesture = Gtk.GestureClick()
		gesture.set_button(0)
		if data:
			gesture.connect("pressed", callback, data)
		else:
			gesture.connect("pressed", callback)
		widget.add_controller(gesture)

	def on_file_double_click(self, gesture, n_press, x, y):
		if n_press == 2:  # Double click
			pos = self.file_selection.get_selected()
			if pos is not None:
				item = self.file_selection.get_item(pos)
				open_file(item.path)

	def on_directory_double_click(self, gesture, n_press, x, y):
		if n_press == 2:  # Double click
			pos = self.dir_selection.get_selected()
			if pos is not None:
				item = self.dir_selection.get_item(pos).get_item()
				if self.selected_file:
					self.move(self.selected_file, item)

	def create_sorter(self, column):
		column_title = column.get_title()
		logger.info("Sort by %s", column_title)
		sorter = Gtk.CustomSorter.new(lambda a, b, *u: self.sort_func(a, b, column_title))
		return sorter

	def sort_func(self, a, b, column_title):
		if column_title == "Name":
			value_a = (a.name, a.extension)
			value_b = (b.name, b.extension)
		elif column_title == "Extension":
			value_a = (a.extension, a.name)
			value_b = (b.extension, b.name)
		else:
			value_a = getattr(a, column_title.lower().replace(" ", "_"))
			value_b = getattr(b, column_title.lower().replace(" ", "_"))

		#logger.info("Compare %s <-> %s", value_a, value_b)
		if value_a < value_b:
			return -1
		elif value_a > value_b:
			return 1
		return 0

	def setup_factory(self, factory, item, column_title):
		box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
		label = Gtk.Label()
		if column_title in ("Name", "Preview"):
			label.set_wrap(True)
		if column_title == "Size":
			label.set_halign(Gtk.Align.END)
		box.append(label)
		item.set_child(box)

	def bind_factory(self, factory, item, column_title):
		file_item = item.get_item()
		box = item.get_child()
		#box.remove(box.get_first_child())
		label = box.get_first_child()
		#logger.info("label: %s", label)
		font_desc = self.file_view_font_desc
		attr_list = Pango.AttrList()
		attr_list.insert(Pango.attr_font_desc_new(font_desc))

		if column_title == "Name":
			#label = Gtk.Label()
			label.set_text(file_item.name)
			label.set_attributes(attr_list)
			#box.append(label)
			self.add_gesture_click(label, self.on_file_double_click)
		elif column_title == "Extension":
			#label = Gtk.Label()
			label.set_text(file_item.extension)
			label.set_attributes(attr_list)
			#box.append(label)
		elif column_title == "Modification Date":
			#label = Gtk.Label()
			label.set_text(self.format_modification_time(file_item.modification_date))
			label.set_attributes(attr_list)
			#box.append(label)
		elif column_title == "Size":
			#label = box.label#Gtk.Label()
			label.set_text(self.format_file_size(file_item.size))
			label.set_attributes(attr_list)
			column = self.columnviews[column_title]
			#label.set_halign(Gtk.Align.END)
			#renderer = Gtk.CellRendererText()
			#def size_column_data_func(self, column, cell, model, iter, data):
			#	cell.set_property("xalign", 1.0)
			#column.set_cell_data_func(renderer, text_column_data_func)
			#box.append(label)
		elif column_title == "Preview":
			#label = Gtk.Label()
			label.set_text(file_item.preview)
			#box.append(label)
		elif column_title == "Proposal":
			#label = Gtk.Label()
			label.set_text(file_item.proposal)
			#box.append(label)
			self.add_gesture_click(label, self.on_proposal_double_click, file_item)

	def on_proposal_double_click(self, gesture, n_press, x, y, file_item):
		if n_press == 2:  # Double click
			self.move(file_item, file_item.proposal)

	def on_proposal_changed(self, combo, file_item):
		file_item.proposal = combo.get_active_text()

	def get_proposals(self, file_item):
		# TODO Example implementation
		return ["Proposal1", "Proposal2", "Proposal3"]

	def calculate_text_width(self, text, font_desc):
		layout = self.create_pango_layout(text)
		layout.set_font_description(font_desc)
		width, _ = layout.get_pixel_size()
		return width

	def format_modification_time(self, timestamp):
		return GLib.DateTime.new_from_unix_local(timestamp).format("%Y-%m-%d %H:%M:%S")

	def format_file_size(self, size):
		for unit in ['B', 'KB', 'MB', 'GB', 'TB']:
			if size < 1024:
				return f"{size:8.2f} {unit}"
			size /= 1024

	def on_file_selected(self, selection, a, b):
		pos = selection.get_selected()
		if pos is not None:
			self.selected_file = self.file_selection.get_item(pos)
		else:
			self.selected_file = None
		logger.info("Selected %s", self.selected_file)

	def on_directory_selected(self, selection, a, b): # 4 arguments needed
		model = selection.get_model()
		selected = selection.get_selected()
		if selected:
			item = model.get_item(selected).get_item()
			self.selected_directory = item
		else:
			self.selected_directory = None
		if self.selected_file:
			self.move(self.selected_file, self.selected_directory)

	def move(self, src, dst):
		logger.info("Moving %s to %s", src, dst)
		# For now we don't actually move, as we're testing the software
		focus_widget = self.get_focus()
		logger.info("Focus on %s", focus_widget)
		selected_position = self.file_selection.get_selected()

		cursor_position = self.file_view.get_cursor()

		logger.info("Cursor position %s", cursor_position)

		src_path = Path(src.path)
		dst_path = Path(dst.path)
		dst_path.mkdir(parents=True, exist_ok=True)
		src_path.rename(dst_path / src_path.name)

		self.set_title(f"PowerMover ({self.file_store.get_n_items()})")

		found, pos = self.file_store.find(src)
		logger.info("pos: %s", pos)
		self.file_store.remove(pos)
		# TODO limit to file store size - 1
		try:
			#self.file_selection.set_selected(selected_position)
			#selected_position = self.file_selection.get_selected()
			logger.info("Select %s", selected_position)
			#self.set_focus(focus_widget)
			#self.file_view.set_cursor(cursor_position)
			#list_item = self.file_view.get_item(selected_position)
			#child = list_item.get_child()
			#child.grab_focus()
			flags = (
			 Gtk.ListScrollFlags.SELECT | #GTK_LIST_SCROLL_SELECT |
			 Gtk.ListScrollFlags.FOCUS #GTK_LIST_SCROLL_FOCUS
			)
			self.file_view.scroll_to(selected_position, None, flags, None)
		except Exception as e:
			logger.exception("Coudln't move %s", e)
			pass

	def populate_src(self):
		self.file_store.remove_all()
		for file in self.source_folder.iterdir():
			if file.is_file():
				if file.name.startswith(".") and not self.config.get("show-hidden", False):
					continue
				file_item = FileItem(
					path=str(file),
					name=file.stem,
					extension=file.suffix,
					modification_date=file.stat().st_mtime,
					size=float(file.stat().st_size),
					preview=self.get_preview(str(file)),
					proposal="Proposal" # TODO
				)
				self.file_store.append(file_item)
		self.set_title(f"Powermover ({self.file_store.get_n_items()})")

	def populate_dst(self):
		"""
		Populate tree root items
		"""
		self.dir_store.remove_all()
		for index, (name, data) in enumerate(self.destinations.items()):
			parent_name = data.get('parent')
			if parent_name is not None:
				continue
			shortcut = self.shortcuts.get(index, '')
			path = str(Path(data['path']).expanduser())
			display_name = f"{name} ({shortcut})" if shortcut else name
			dest_item = DestinationItem(
				name=name,
				display_name=display_name,
				path=path,
				parent=None,
			)
			self.shortcut_to_path[index] = dest_item
			self.dir_store.append(dest_item)

	def create_tree_func(self, item):
		"""
		Populate items under `item`
		"""
		logger.debug("Create tree func %s", item)

		children = Gio.ListStore(item_type=DestinationItem)
		for index, (name, data) in enumerate(self.destinations.items()):
			parent_name = data.get('parent')
			if parent_name != item.name:
				continue
			shortcut = self.shortcuts.get(index, '')
			path = str(Path(data['path']).expanduser())
			display_name = f"{name} ({shortcut})" if shortcut else name
			dest_item = DestinationItem(
				name=name,
				display_name=display_name,
				path=path,
				parent=item,
			)
			children.append(dest_item)
			self.shortcut_to_path[index] = dest_item

		return children

	def create_tree_factory(self):
		factory = Gtk.SignalListItemFactory()
		factory.connect("setup", self.setup_tree_factory)
		factory.connect("bind", self.bind_tree_factory)
		return factory

	def setup_tree_factory(self, factory, list_item):
		expander = Gtk.TreeExpander()
		label = Gtk.Label()
		expander.set_child(label)
		list_item.set_child(expander)

	def bind_tree_factory(self, factory, list_item):
		item = list_item.get_item()
		expander = list_item.get_child()
		expander.set_list_row(item)
		label = expander.get_child()
		destination_item = item.get_item()
		label.set_text(destination_item.display_name)


class PowerMoverApp(Gtk.Application):
	def __init__(self):
		super().__init__(application_id="eu.zougloub.powermover", flags=Gio.ApplicationFlags.FLAGS_NONE)

	def do_activate(self):
		win = PowerMover(self)
		win.present()

	def do_startup(self):
		Gtk.Application.do_startup(self)


def main():
	import argparse
	parser = argparse.ArgumentParser(
		description="IMAP Client",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	args = parser.parse_args()

	logger.setLevel(getattr(logging, args.log_level))

	logging.basicConfig(
	 format="%(levelname)s %(message)s"
	)

	app = PowerMoverApp()
	app.run(None)


if __name__ == "__main__":
	main()
