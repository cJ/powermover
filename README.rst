##########
PowerMover
##########

You may be using Midnight Commander, setting up a dispatching folder
with symlinks to various destinations, and then perform dispatches by doing:

#. tab to go to left pane
#. up/down to select file
#. tab to go to right pane
#. select destination directory, enter it
#. tab to go to left pane
#. F6 to move file
#. tab to go to right pane
#. home/enter to go back to the dispatch folder
#. GOTO 1

Well, PowerMover is a GUI that can be used to dispatch content listed on the
left pane, to destinations on the right pane, using a minimum of keyboard shortcuts
for fastest operation.


In the source pane:

- Identifier (eg. file path stem/extension)
- Properties (eg. file size, mtime)
- Preview (currently a cached textual preview from lessopen)

In the destination pane: a tree of destinations; a tree structure is
available because it makes sense for sorting.


Typically used to organize chaos in `~/Downloads`, sending files into
"proper" locations.
-



Usage
#####

.. admonition:: Warning

   You do not want to use this software with pets or little children
   roaming around.

Once `configuration`_ is done, PowerMover can be used.

It is keyboard-driven:

- up/down moves selection on the source pane;

- 0-9, a-z will *move* content to the destination marked with the
  corresponding shortcut;

- space (not implemented) will *move* content to the automagically
  proposed destination;

- enter / return will *open* the source (so you can inspect it further).


Configuration
#############

Specify `POWERMOVER_CONFIG` in environment, defaulting to
`~/.config/powermover.yml`, which contains for example:

.. code:: yaml

  source: ~/Downloads #

  destinations:

    Trash:
      path: ~/Downloads/Trash

    Electronics:
      path: ~/Electronics
    Electronics - Datasheets:
      parent: Electronics
      path: ~/Electronics/DS
    Electronics - User Guides:
      parent: Electronics
      path: ~/Electronics/UG
    Electronics - Application Notes:
      parent: Electronics
      path: ~/Electronics/AN
    Papers:
      path: ~/Papers

License
#######

GPLv3.

